package app;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AdderTests {

    @Test
    public void doesAdderAdd() {
        Adder adder = new Adder();
        assertEquals(0, adder.add(0));
        assertEquals(1, adder.add(1));
        assertEquals(0, adder.add(-1));
    }


}
